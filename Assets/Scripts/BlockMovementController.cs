﻿using UnityEngine;
using System.Collections;

public class BlockMovementController : MonoBehaviour {

	public bool _isMoving = false;
	public Ray _frontRay;
	public Ray _backRay;
	public Ray _rightRay;
	public Ray _leftRay;
	public Ray _upRay;
	public Ray _downRay;

	public Ray _frontRightRay;
	public Ray _frontLeftRay;
	public Ray _frontDownRay;
	public Ray _twoForwardRay;

	public bool isMoving
	{
		get
		{
			return _isMoving;
		}
		set
		{
			if(_isMoving && !value)
			{
				CheckRays();
			}
			_isMoving = value;
			
		}
	}

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public void CheckRays()
	{
		_frontRay = new Ray(transform.position, transform.forward);
		_backRay = new Ray(transform.position, -transform.forward);
		_rightRay = new Ray(transform.position, transform.right);
		_leftRay = new Ray(transform.position, -transform.right);
		_upRay = new Ray(transform.position, transform.up);
		_downRay = new Ray(transform.position, -transform.up);

		_frontRightRay = new Ray(transform.position + transform.forward, transform.right);
		_frontLeftRay = new Ray(transform.position + transform.forward, -transform.right);
		_frontDownRay = new Ray(transform.position + transform.forward, -transform.up);
		_twoForwardRay = new Ray(transform.position + transform.forward, transform.forward);
	}
}
