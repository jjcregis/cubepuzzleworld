﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Camera[] _cameras;
	public int _activeCamera;

	// Use this for initialization
	void Start ()
	{
		_activeCamera = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		HandleCameraChanges();
	}

	void HandleCameraChanges()
	{
		int newCameraIndex = -1;
		
		// Change cameras
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			newCameraIndex = 1;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			newCameraIndex = 2;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha3))
		{
			newCameraIndex = 3;
		}
		if(newCameraIndex == _activeCamera)
		{
			newCameraIndex = 0;
		}
		if(newCameraIndex >= 0)
		{
			ActivateCamera(newCameraIndex);
		}
	}

	void ActivateCamera(int index)
	{
		if(_activeCamera != 0 && index != 0)
		{
			ActivateCamera(0);
			ActivateCamera(index);
		}

		else
		{
			// Swap the camera viewports
			Rect tempRect = _cameras[_activeCamera].rect;
			_cameras[_activeCamera].rect = _cameras[index].rect;
			_cameras[index].rect = tempRect;
			
			// Change the AudioListener
			_cameras[_activeCamera].GetComponent<AudioListener>().enabled = false;
			_cameras[index].GetComponent<AudioListener>().enabled = true;
			
			_activeCamera = index;
		}
	}
}
