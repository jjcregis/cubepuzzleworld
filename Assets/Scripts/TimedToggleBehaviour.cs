﻿using UnityEngine;
using System.Collections;

public class TimedToggleBehaviour : OnOffButtonBehaviour {

	public float toggleTime = .25f;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public IEnumerator Toggle()
	{
		base.Toggle();
		yield return new WaitForSeconds(toggleTime);
		base.Toggle();
	}
}
