﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BlockMovementController))]
[RequireComponent(typeof(MoveBehaviour))]
public class FallBehaviour : MonoBehaviour {

	public bool shouldFall;
	private BlockMovementController controller;
	private MoveBehaviour move;

	
	// Use this for initialization
	void Start ()
	{
		controller = gameObject.GetComponent<BlockMovementController>();
		move = gameObject.GetComponent<MoveBehaviour>();
		controller.CheckRays();
	}
	
	// Update is called once per frame
	void Update ()
	{
		StartCoroutine(Fall());
		shouldFall = ShouldFall();
	}
	
	public IEnumerator Fall()
	{
		if(shouldFall)
		{
			yield return StartCoroutine(move.MoveUp(true));
		}
        else
        {
            yield return null;
        }
    }

	public bool ShouldFall()
	{
		return !Physics.Raycast(controller._downRay, 1) && !controller.isMoving;
	}
}
