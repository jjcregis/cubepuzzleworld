﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BlockMovementController))]
public class RotateBehaviour : MonoBehaviour
{

	public float secondsPerRotation = .25f;
	private BlockMovementController controller;


	// Use this for initialization
	void Start ()
	{
		controller = gameObject.GetComponent<BlockMovementController>();	
	}
	

	// Update is called once per frame
	void Update ()
	{	
	}
	

	public IEnumerator Turn90Degrees(bool clockwise)
	{
		if(!controller.isMoving)
		{
			float angle = 90;
			if(!clockwise)
			{
				angle *= -1;
			}

			yield return StartCoroutine(RotateFromTo(transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.y + angle, secondsPerRotation));
		}
	}


	IEnumerator RotateFromTo(float startAngle, float endAngle, float time)
	{
		controller.isMoving = true;

		Vector3 angles;
		float t = 0f;
		while (t < 1f){
			t += Time.deltaTime / time;
			angles = transform.localEulerAngles;
			angles.y = Mathf.Lerp(startAngle, endAngle, t);
			transform.localEulerAngles = angles;
			yield return 0;
		}

		controller.isMoving = false;
		//RecalculateRays();
		//CheckForGroundedHang();
	}
}
