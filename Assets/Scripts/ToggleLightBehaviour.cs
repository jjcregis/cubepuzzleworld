﻿using UnityEngine;
using System.Collections;

public class ToggleLightBehaviour : OnOffBehaviour {

	Light[] lights;

	// Use this for initialization
	void Start ()
	{
		lights = GetComponentsInChildren<Light>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach(Light light in lights)
		{
			light.enabled = isOn;
		}
	}
}
