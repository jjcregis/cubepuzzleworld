﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BlockMovementController))]
public class MoveBehaviour : MonoBehaviour
{
	public float secondsPerMovement = .25f;
	public float unitsPerMovement = 1;

	private BlockMovementController controller;

	// Use this for initialization
	void Start ()
	{
		controller = gameObject.GetComponent<BlockMovementController>();
	}
	

	// Update is called once per frame
	void Update ()
	{
	}
	

	IEnumerator MoveFromTo(Vector3 startPosition, Vector3 endPosition, float time)
	{
		controller.isMoving = true;

		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / time;
			transform.position = Vector3.Lerp(startPosition, endPosition, t);
			yield return null;
		}

		controller.isMoving = false;
	}

	public IEnumerator MoveForward(bool reverse)
	{
		yield return StartCoroutine(MoveForward(reverse, false));
    }

    public IEnumerator MoveForward(bool reverse, bool lookAhead)
	{
		if(!controller.isMoving)
		{
			Ray ray = lookAhead ? controller._twoForwardRay : controller._frontRay;
			float distance = unitsPerMovement;
			if(reverse)
			{
				ray = controller._backRay;
				distance *= -1;
			}
			if(!Physics.Raycast(ray, unitsPerMovement))
			{
            	yield return StartCoroutine(MoveFromTo(transform.position, transform.position + transform.forward * distance, secondsPerMovement));
			}

			yield return null;
		}
	}

	
	public IEnumerator MoveUp(bool reverse)
	{
		if(!controller.isMoving)
		{
			Ray ray = controller._upRay;
			float distance = unitsPerMovement;
			if(reverse)
			{
				ray = controller._downRay;
				distance *= -1;
			}
			if(!Physics.Raycast(ray, unitsPerMovement))
			{
				yield return StartCoroutine(MoveFromTo(transform.position, transform.position + transform.up * distance, secondsPerMovement));
			}

			yield return null;
        }
    }

	public IEnumerator MoveTo(Vector3 toPosition)
	{
		if(!controller.isMoving)
		{			
			yield return StartCoroutine(MoveFromTo(transform.position, toPosition, secondsPerMovement));
		}
	}
}
