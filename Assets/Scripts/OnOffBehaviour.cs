﻿using UnityEngine;
using System.Collections;

public class OnOffBehaviour : MonoBehaviour {

	public bool isOn;
	public bool changeMaterialOnToggle;
	public Material offMaterial;
	public Material onMaterial;

	// Use this for initialization
	void Start ()
	{
		if(changeMaterialOnToggle)
		{
			renderer.material = isOn ? onMaterial : offMaterial;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Toggle()
	{
		isOn = !isOn;
		if(changeMaterialOnToggle)
		{
			renderer.material = isOn ? onMaterial : offMaterial;
		}
	}
}
