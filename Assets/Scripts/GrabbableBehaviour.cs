﻿using UnityEngine;
using System.Collections;

public class GrabbableBehaviour : MonoBehaviour {

	public bool _isHeld = false;
	private BlockMovementController _controller;
	
	public bool isHeld
	{
		get
		{
			return _isHeld;
		}
		set
		{
			_isHeld = value;
			_controller.CheckRays();
		}
	}

	// Use this for initialization
	void Start ()
	{
		_controller = gameObject.GetComponent<BlockMovementController>();
	}
	
	// Update is called once per frame
	void Update ()
	{	
	}
}
