﻿using UnityEngine;
using System.Collections;

public class OnOffButtonBehaviour : OnOffBehaviour
{
	public OnOffBehaviour[] machines;

	// Use this for initialization
	void Start ()
	{
		//UpdateMachines();
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
	
	public void Toggle()
	{
		base.Toggle();
		UpdateMachines();
	}

	void UpdateMachines()
	{
		if(machines != null)
		{
			for(int i=0; i<machines.Length; i++)
			{
				machines[i].Toggle();
			}
		}
	}
}
