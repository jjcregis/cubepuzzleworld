﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FallBehaviour))]
[RequireComponent(typeof(MoveBehaviour))]
[RequireComponent(typeof(RotateBehaviour))]
public class PlayerBehaviour : MonoBehaviour
{
	// Behaviours
	private MoveBehaviour move;
	private BlockMovementController controller;
	private RotateBehaviour rotate;
	private FallBehaviour fall;

	// Keys
	public KeyCode _useKey = KeyCode.R;
	public KeyCode _rotateItemRight = KeyCode.E;
	public KeyCode _rotateItemLeft = KeyCode.Q;
	public KeyCode _jumpKey = KeyCode.Space;

	public float SecondsPerMovement = .25f;
	public float UnitsPerMovement = 1;
	
	public GrabbableBehaviour _heldObject;
	
	// Use this for initialization
	void Start ()
	{
		controller = gameObject.GetComponent<BlockMovementController>();
		move = gameObject.GetComponent<MoveBehaviour>();
		rotate = gameObject.GetComponent<RotateBehaviour>();
		fall = gameObject.GetComponent<FallBehaviour>();
		controller.CheckRays();
	}
	
	void Hold(GrabbableBehaviour holdableObject)
	{
		_heldObject = holdableObject;
		_heldObject.transform.parent = transform;
		_heldObject.transform.position = transform.position + transform.forward;
		_heldObject.isHeld = true;
    }

	void Release()
	{
		_heldObject.transform.parent = null;
		_heldObject.isHeld = false;
		_heldObject = null;
	}
	
	IEnumerator TurnClockwise(bool reverse)
	{
		yield return StartCoroutine(rotate.Turn90Degrees(reverse));
	}

	IEnumerator MoveForward(bool reverse)
	{
		yield return StartCoroutine(move.MoveForward(reverse, _heldObject != null));
	}

	IEnumerator MoveUp(bool reverse)
	{
		yield return StartCoroutine(move.MoveUp(reverse));
	}

	IEnumerator Jump()
	{
		yield return StartCoroutine(MoveUp(false));
		yield return StartCoroutine(MoveForward(false));
	}

	void Update()
	{
		float xMovement = Input.GetAxis ("Horizontal");
		float yMovement = Input.GetAxis ("Vertical");
		

		
		// Set the position to move to and update direction. Only move if the previous movement has finished
		if (!controller.isMoving)
		{
			Ray ray;
			RaycastHit rayHit;
			bool blocked = false;

			// Check for obstacles if block is rotating
			if (xMovement != 0)
			{
				// Check if the held object will collide with anything during rotation
				if(_heldObject != null)
				{
					if(xMovement > 0)
					{
						blocked = Physics.Raycast(controller._rightRay, 1) || Physics.Raycast(controller._frontRightRay, 1);
					}
					else
					{
						blocked = Physics.Raycast(controller._leftRay, 1) || Physics.Raycast(controller._frontLeftRay, 1);
					}
				}
				if(!blocked)
				{
					StartCoroutine(TurnClockwise((xMovement > 0)));
				}				
			}
			else if (yMovement != 0)
			{
				StartCoroutine(MoveForward(yMovement < 0));
            }

			// Grab or release an object in front of us
			else if(Input.GetKeyDown(_useKey))
			{
				HandleUseKey();
            }

			// Jump
			else if(Input.GetKeyDown(_jumpKey))
			{
				StartCoroutine(Jump());
			}

			// Rotate objects
			else if(Input.GetKeyDown(_rotateItemRight))
			{
				RotateBehaviour rotate = _heldObject.GetComponent<RotateBehaviour>();
				if(rotate != null)
				{
					StartCoroutine(rotate.Turn90Degrees(true));
				}
			}
			else if(Input.GetKeyDown(_rotateItemLeft))
			{
				RotateBehaviour rotate = _heldObject.GetComponent<RotateBehaviour>();
				if(rotate != null)
				{
					StartCoroutine(rotate.Turn90Degrees(false));
				}
			}
        }

		if(_heldObject != null)
		{
			if(fall.shouldFall)
			{
				// Don't fall if the held object is holding us up
				fall.shouldFall = !Physics.Raycast(controller._frontDownRay, 1) && fall.ShouldFall();
			}

			// Held object shouldn't fall if held over an edge
			FallBehaviour heldFall = _heldObject.GetComponent<FallBehaviour>();
			if(heldFall != null)
			{
				heldFall.shouldFall = false;
			}
		}
    }
	
	void HandleUseKey()
	{
		if(_heldObject != null)
		{
			Release();
		}
		else
		{
			RaycastHit frontRayHit;
			Physics.Raycast(controller._frontRay, out frontRayHit, 1.0f);
			
			// Check if a usable object is in front of us
			if(frontRayHit.transform != null && frontRayHit.transform.gameObject != null)
			{
				// Check if it's grabbable
				GrabbableBehaviour grabbable = frontRayHit.transform.gameObject.GetComponent<GrabbableBehaviour>();
				if(grabbable != null)
				{
					Hold(grabbable);
					return;
				}
				
				// Check if it's a timed toggleable
				TimedToggleBehaviour timedToggle = frontRayHit.transform.gameObject.GetComponent<TimedToggleBehaviour>();
				if(timedToggle != null)
				{
					StartCoroutine(timedToggle.Toggle());
					return;
				}
				
				// Check if it's toggleable
				OnOffButtonBehaviour onOff = frontRayHit.transform.gameObject.GetComponent<OnOffButtonBehaviour>();
				if(onOff != null)
				{
					onOff.Toggle();
					return;
				}
			}
		}
	}
}