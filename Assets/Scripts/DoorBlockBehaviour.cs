﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveBehaviour))]
[RequireComponent(typeof(BlockMovementController))]
public class DoorBlockBehaviour : OnOffBehaviour
{
	private MoveBehaviour move;
	private BlockMovementController controller;
	
	private Vector3 offPosition;
	public Vector3 onOffset;
	
	//public bool isOpen = false;

	// Use this for initialization
	void Start ()
	{
		move = GetComponent<MoveBehaviour>();
		controller = GetComponent<BlockMovementController>();

		changeMaterialOnToggle = false;
		offPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		StartCoroutine(Open());
	}

	public IEnumerator Open()
	{
		if(isOn && !controller.isMoving)
		{
			yield return StartCoroutine(move.MoveTo(transform.position + onOffset));
		}/*
		else if (!isOn && !controller.isMoving)
		{
			yield return StartCoroutine(move.MoveTo(transform.position - onOffset));
		}*/
		else
		{
			yield return null;
		}
	}
}
